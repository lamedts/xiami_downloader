"""
xiami downloader
Encoding problem when using windows terminals
song 1771480635
album 417348
collecction 1769165575
chinese collection 1769165575
chinese song 398237
"""
# -*- coding: utf8 -*-
import re, urllib, urllib2, json, os
import sys, getopt
import codecs

from mutagen.id3 import ID3,TRCK,TIT2,TALB,TPE1,APIC,TDRC,COMM,TPOS,USLT, ID3NoHeaderError

import utils.console
from utils.console import current_os
from utils.langconv import *

proxy_ip = "117.135.252.14"
proxy_port = "80"
proxies = {"http":"http://%s:%s" % (proxy_ip, proxy_port)}
headers = {'User-agent' : 'Mozilla/5.0'}
source = "xiami"
dlType = 0

sys.stdout = codecs.getwriter('utf8')(sys.stdout, 'strict')

def set_dir():
    if current_os == 'Windows':
        destination = "c:/%s/" % source
    if current_os in ['Linux', 'Darwin'] or current_os.startswith('CYGWIN'):
        from os.path import expanduser
        destination = expanduser("~")
        destination += "/%s/" % source
    if not os.path.exists(destination):
        os.makedirs(destination)
    return destination

def get_real_url(self):
    strlen = len(self[1:])
    rows = int(self[0])
    cols = strlen / rows
    right_rows = strlen % rows
    new_str = self[1:] 
    url_true = ''                          
    for i in xrange(strlen):
        x = i % rows
        y = i / rows
        p = 0
        if x <= right_rows:
            p = x * (cols + 1) + y
        else:
            p = right_rows * (cols + 1) + (x - right_rows) * cols + y
        url_true += new_str[p]
    return urllib2.unquote(url_true).replace('^', '0')

def get_obj(url):
    proxy_support = urllib2.ProxyHandler(proxies)
    opener = urllib2.build_opener(proxy_support)
    #opener = urllib2.build_opener(proxy_support, urllib2.HTTPHandler(debuglevel=1))
    urllib2.install_opener(opener)
    req = urllib2.Request(url, None, headers)
    response = urllib2.urlopen(req)
    obj = json.load(response)
    response.close()
    return obj

def refresh_console(header, dling_output, finishDL):
    console.cls()
    body = header + dling_output
    sys.stdout.write(body)
    sys.stdout.write(finishDL)

def get_song_json(location, json_url):
    obj_json = get_obj(json_url)

    (width, height) = console.get_terminal_size()
    sep = u'%s\n'% ('=' * width)
    line = u'%s\n'% ('+' * width)
    newLine = u"\n"
    finishDL = newLine + sep
    
    for i, j in enumerate(obj_json['data']['trackList']):
        console.cls()
        song_name = j['title']
        song_artist = j['artist']
        song_album = j['album_name']
        song_name = Converter('zh-hant').convert(song_name)         # decode to unicode     from utf-8(str)
        #song_name = song_name.encode('utf-8')                      # encode to utf-8(str)  from unicode
        song_artist = Converter('zh-hant').convert(song_artist)
        #song_artist = song_artist.encode('utf-8')
        song_album = Converter('zh-hant').convert(song_album)
        #song_album = song_album.encode('utf-8')
        trackNo = i + 1

        song_file = u"%d. %s - %s.mp3" % (i + 1, song_artist, song_name)
        if i == 0:
            if dlType == 0:
                folder = song_artist + " - " + song_album
                location = os.path.join(location, folder) 
                if not os.path.exists(location):
                    os.makedirs(location)
        path = os.path.join(location, song_file)
        dlurl = get_real_url(j['location'])
        head1  = u"\tOS: %s          dir: %s      Proxy: %s:%s\n" % (current_os, location, proxy_ip, proxy_port)
        head2 = u"\t[%d/%d]\n" % (trackNo, len(obj_json))
        
        header = line + newLine + head1 + head2 + newLine + line  
        #dling = u"\t%s\n" % dlurl
        
        #print dlurl
        retry = 1
        while True:
            try:
                dling = u"Downloading:\n\t%s \t%s\n" % (song_artist, song_name)
                dling += u"\t-->Start \n"
                dling_output = dling + newLine + newLine + newLine 
                refresh_console(header, dling_output, finishDL)

                dlreq = urllib2.Request(dlurl, None, headers)

                dling += u"\t-->Requested \n"
                dling_output = dling + newLine + newLine                
                refresh_console(header, dling_output, finishDL)

                download = urllib2.urlopen(dlreq,timeout=10) 

                dling += u"\t-->Downloading \n" 
                dling_output = dling + newLine             
                refresh_console(header, dling_output, finishDL)

                open(path,"wb").write(download.read())     
                dling += u"\t-->Finish\n"
                dling_output = dling       
                refresh_console(header, dling_output, finishDL)

                finishDL += u"\t[DONE] - %d. %s - %s\n" % (i + 1, song_artist, song_name)
                refresh_console(header, dling_output, finishDL)
                break
            except Exception, e:
                sys.stdout.write(u"\t\t-->%s, Retry %d\n" % (e, retry))
                retry = retry + 1
                pass
            finally:
                download.close()
        writeTag(path, song_artist, song_album, song_name, trackNo)

def writeTag(filepath, artist, album, song, num):
    try:
        tags = ID3(filepath)
    except ID3NoHeaderError:
        tags = ID3()
    tags.add(TIT2(encoding=3, text= song))
    tags.add(TALB(encoding=3, text= album))
    tags.add(TPE1(encoding=3, text= artist))
    tags.add(TRCK(encoding=3, text= str(num)))
    tags.save(filepath, v2_version=3)
