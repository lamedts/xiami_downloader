# xiami_downloader
A simple tool for downloading music previews from Xiami.com

Note: this is a python script using python 2.

#Dependency
Optionally depends on **mutagen** module for ID3 tag support


#Usage

python xiami.py [options]
#Options

* -a <album id> Adds all songs in an album to download list.

* -c <collection id> Adds all songs in a playlist to download list.

* -s <song id> Adds a song to download list.

* -h Shows usage.

