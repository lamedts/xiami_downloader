
# -*- coding: utf8 -*-
import re, urllib, urllib2, json, os
import sys, getopt
import codecs

from mutagen.id3 import ID3,TRCK,TIT2,TALB,TPE1,APIC,TDRC,COMM,TPOS,USLT, ID3NoHeaderError

from  utils import console
from  xiami import xiami

from utils.console import current_os
from utils.langconv import *

def usage():
    print "Description:"
    print "\tA simple python program to download the song form xiami"
    print "\nUsage:"
    print "\t_xiami.py [OPTION]... ID"
    print "\nOptions:"
    print "\t-h, --help           print this help"
    print "\t-s, --song           Download a  song by id"
    print "\t-a, --album          Download an album by id"
    print "\t-c, --collection     Download a  collection by id"

def main(argv):
    flag = False
    console.cls()
    try:
        opts, args = getopt.getopt(argv,"ha:c:s:",["help","album=","collection=", "song="])
    except getopt.GetoptError:
        print ("getopt error!")
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            flag = True
            sys.exit()
        elif opt in ("-a", "--album"):
            dlType = 0    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/type/1/cat/json?_ksTS=1423711851040_1429" % arg
        elif opt in ("-c", "--collection"):
            dlType = 1    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/type/3/cat/json?_ksTS=1422948834823" % arg
        elif opt in ("-s", "--song"):
            dlType = 2    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/object_name/default/object_id/0/cat/json?_ksTS=1423751499433_1209" % arg
    if flag == False:
        xiami.get_song_json(xiami.set_dir(), json_url) 

if __name__ == "__main__":
   main(sys.argv[1:])